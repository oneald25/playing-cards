// Part 1 of Lab2 "Playing Cards"
// Dan


#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank
{
	ONE,
	TWO,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE

};

enum Suit

{
	HEARTS,
	DIAMONDS,
	CLOVERS,
	SPADES
};

struct Card
{
	int num;
	Rank rank;
	Suit suit;
	int value;
	string faceValue;

};


Rank AssignRank(Card c, int pcRank)
{
	switch (pcRank)
	{

	case 1:
		return c.rank = ONE;
		break;

	case 2:
		return c.rank = TWO;
		break;

	case 3:
		return c.rank = THREE;
		break;

	case 4:
		return c.rank = FOUR;
		break;

	case 5:
		return c.rank = FIVE;
		break;

	case 6:
		return c.rank = SIX;
		break;

	case 7:
		return c.rank = SEVEN;
		break;

	case 8:
		return c.rank = EIGHT;
		break;

	case 9:
		return c.rank = NINE;
		break;

	case 10:
		return c.rank = TEN;
		break;

	case 11:
		return c.rank = JACK;
		break;

	case 12:
		return c.rank = QUEEN;
		break;

	case 13:
		return c.rank = KING;
		break;

	case 14:
		return c.rank = ACE;
		break;
	}

}


string AssignRankNames(Card c, int j)
{
	switch (j)
	{

	case 0:
		return c.faceValue = "One";
		break;

	case 1:
		return c.faceValue = "Two";
		break;

	case 2:
		return c.faceValue = "Three";
		break;

	case 3:
		return c.faceValue = "Four";
		break;

	case 4:
		return c.faceValue = "Five";
		break;

	case 5:
		return c.faceValue = "Six";
		break;

	case 6:
		return c.faceValue = "Seven";
		break;

	case 7:
		return c.faceValue = "Eight";
		break;

	case 8:
		return c.faceValue = "Nine";
		break;

	case 9:
		return c.faceValue = "Ten";
		break;

	case 10:
		return c.faceValue = "Jack";
		break;

	case 11:
		return c.faceValue = "Queen";
		break;

	case 12:
		return c.faceValue = "King";
		break;

	case 13:
		return c.faceValue = "Ace";
		break;
	}

}


int main()

{
	int n = 1, nmax = 52;
	int i=1, imax = 14;
	string suiteNames = "", rankNames = "";
	Card pc;
	

	for (n = 0; n <= 55; n++)
	{

		if (n <= 13)
		{
			pc.num = n;
			pc.suit = HEARTS;
			suiteNames = "Hearts";
			i = (n % imax) + 1;
			pc.rank = AssignRank(pc, i);
			pc.faceValue = AssignRankNames(pc, i-1);
			rankNames = pc.faceValue;
			pc.value = i;

			cout << pc.suit << ", " << pc.rank << ", " << pc.value << "\n";
			cout << suiteNames << ", " << rankNames << ", " << pc.value << "\n";
			cout << "\n";

		}

		else if (n >= 14 && n <= 27)
		{
			pc.num = n;
			pc.suit = DIAMONDS;
			suiteNames = "Diamonds";
			i = (n % imax) + 1;
			pc.rank = AssignRank(pc, i);
			pc.faceValue = AssignRankNames(pc, i-1);
			rankNames = pc.faceValue;
			pc.value = i;

			cout << pc.suit << ", " << pc.rank << ", " << pc.value << "\n";
			cout << suiteNames << ", " << rankNames << ", " << pc.value << "\n";
			cout << "\n";


		}

		else if (n >= 28 && n <= 41)
		{
			pc.num = n;
			pc.suit = CLOVERS;
			suiteNames = "Clovers";
			i = (n % imax) + 1;
			pc.rank = AssignRank(pc, i);
			pc.faceValue = AssignRankNames(pc, i - 1);
			rankNames = pc.faceValue;
			pc.value = i;

			cout << pc.suit << ", " << pc.rank << ", " << pc.value << "\n";
			cout << suiteNames << ", " << rankNames << ", " << pc.value << "\n";
			cout << "\n";

		}

		else if (n >= 42 && n <= 55)

		{
			pc.num = n;
			pc.suit = SPADES;
			suiteNames = "Spades";
			i = (n % imax) + 1;
			pc.rank = AssignRank(pc, i);
			pc.faceValue = AssignRankNames(pc, i - 1);
			rankNames = pc.faceValue;
			pc.value = i;

			cout << pc.suit << ", " << pc.rank << ", " << pc.value << "\n";
			cout << suiteNames << ", " << rankNames << ", " << pc.value << "\n";
			cout << "\n";


		}


	}


	_getch();
	return 0;

}